package apps.ahqmrf.note;

import android.annotation.SuppressLint;
import android.provider.Settings;
import android.widget.Toast;

/**
 * Created by bsse0 on 2/1/2018.
 */

public final class Util {
    @SuppressLint("HardwareIds")
    public static String getHardwareId() {
        return Settings.Secure.getString(App.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static void showToast(String message) {
        Toast.makeText(App.getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
