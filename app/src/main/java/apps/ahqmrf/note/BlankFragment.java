package apps.ahqmrf.note;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {
    View rootView;
    Unbinder unbinder;
    String title;

    @BindView(R.id.textView) TextView label;

    public BlankFragment() {
        // Required empty public constructor
    }

    public static BlankFragment getInstance(String title) {
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        BlankFragment bf = new BlankFragment();
        bf.setArguments(bundle);
        return bf;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.tab_fragment, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        label.setText(getArguments().getString("title"));
        return rootView;
    }

    @Override public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
