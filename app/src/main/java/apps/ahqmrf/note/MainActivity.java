package apps.ahqmrf.note;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    @BindView(R.id.tab_layout) TabLayout mTabLayout;
    @BindView(R.id.viewpager) ViewPager mViewPager;

    PagerAdapter mAdapter;

    int tabIconDefaultColor  = ContextCompat.getColor(App.getContext(), R.color.tabIconDefaultColor);
    int tabIconSelectedColor = ContextCompat.getColor(App.getContext(), R.color.tabIconSelectedColor);
    int icons[]              = {R.drawable.ic_note, R.drawable.ic_money_calculate, R.drawable.ic_settings};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setTabLayout();
    }

    private void setTabLayout() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTabLayout.setElevation(8f);
        }

        for (int i = 0; i < 3; i++) {
            TabLayout.Tab tab = mTabLayout.newTab();
            tab.setIcon(icons[i]);
            Drawable icon = tab.getIcon();
            if(icon != null) icon.setColorFilter(tabIconDefaultColor, PorterDuff.Mode.SRC_IN);
            mTabLayout.addTab(tab);
        }

        mAdapter = new PagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.addOnTabSelectedListener(this);
        if(mTabLayout.getTabAt(0) != null) {
            onTabSelected(mTabLayout.getTabAt(0));
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
        Drawable icon = tab.getIcon();
        if(icon != null) icon.setColorFilter(tabIconSelectedColor, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        Drawable icon = tab.getIcon();
        if(icon != null) icon.setColorFilter(tabIconDefaultColor, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}